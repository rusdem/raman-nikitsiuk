package dbtest.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.lab.nikitsuik.dao.impl.TagDAOImpl;
import com.epam.lab.nikitsuik.entity.Tag;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import dbtest.DAOTest;

@DatabaseSetup("/data-set.xml")
public class TagDAOImplTest extends DAOTest {
	@Autowired
	TagDAOImpl tagDAO;

	@Test
	public void addTag() throws Exception {
		Tag tag = new Tag();
		tag.setId(6L);
		tag.setTagName("Name");
		int countBefore = tagDAO.selectAll().size();
		tagDAO.add(tag);
		assertEquals(tagDAO.selectAll().size(), countBefore + 1);
	}


	@Test
	public void readTag() throws Exception {
		assertEquals(tagDAO.select(2L).getId(), new Long(2L));
	}

	@Test
	public void updateTag() throws Exception {
		Tag tag = new Tag();
		tag.setTagName("Name");
		tagDAO.update(tag);
		assertEquals(tagDAO.select(3L).getTagName(), "Name");
	}


	@Test
	public void deleteTag() throws Exception {
		int countBefore = tagDAO.selectAll().size();
		tagDAO.delete(2L);
		assertEquals(tagDAO.selectAll().size(), countBefore - 1);
	}

	@Test
	public void addTags() throws Exception {
		int countBefore = tagDAO.selectAll().size();
		List<Tag> lst = new ArrayList<Tag>();
		Tag tag = new Tag();
		tag.setTagName("Something");
		lst.add(tag);
		lst.add(tag);
		tagDAO.addTags(lst);
		assertEquals(tagDAO.selectAll().size(), countBefore + 2);
	}
}