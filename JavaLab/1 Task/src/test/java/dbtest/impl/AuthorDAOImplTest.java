package dbtest.impl;

import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.lab.nikitsuik.dao.impl.AuthorDAOImpl;
import com.epam.lab.nikitsuik.entity.Author;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import dbtest.DAOTest;

@DatabaseSetup("/data-set.xml")
public class AuthorDAOImplTest extends DAOTest {
	@Autowired
	AuthorDAOImpl authorDAO;

	@Test
	public void addAuthor() throws Exception {
		Author author = new Author();
		author.setId(6L);
		author.setAuthorName("Alex");
		int countBefore = authorDAO.selectAll().size();
		authorDAO.add(author);
		assertEquals(authorDAO.selectAll().size(), countBefore + 1);
	}


	@Test
	public void readAuthor() throws Exception {
		assertEquals(authorDAO.select(2L).getId(), new Long(2L));
	}

	@Test
	public void updateAuthor() throws Exception {
		Author author = new Author();
		author.setAuthorName("Alex");
		authorDAO.update(author);
		assertEquals(authorDAO.select(3L).getAuthorName(), "Alex");
	}


	@Test
	public void deleteAuthor() throws Exception {
		int countBefore = authorDAO.selectAll().size();
		authorDAO.delete(2L);
		assertEquals(authorDAO.selectAll().size(), countBefore - 1);
	}
}