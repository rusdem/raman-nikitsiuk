package dbtest.impl;

import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.lab.nikitsuik.dao.impl.CommentsDAOImpl;
import com.epam.lab.nikitsuik.entity.Comments;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import dbtest.DAOTest;

@DatabaseSetup("/data-set.xml")
public class CommentsDAOImplTest extends DAOTest {
	@Autowired
	CommentsDAOImpl commentsDAO;
	@Test
	public void addComments() throws Exception {
		Comments comments = new Comments();
		comments.setId(8L);
		comments.setCommentText("TEXT");
		int countBefore = commentsDAO.selectAll().size();
		commentsDAO.add(comments);
		assertEquals(commentsDAO.selectAll().size(), countBefore + 1);
	}

	@Test
	public void readComments() throws Exception {
		assertEquals(commentsDAO.select(2L).getId(), new Long(2L));
	}

	@Test
	public void updateComments() throws Exception {
		Comments comments = new Comments();
		comments.setCommentText("Text");
		commentsDAO.update(comments);
		assertEquals(commentsDAO.select(3L).getCommentText(), "Text");
	}

	@Test
	public void deleteComments() throws Exception {
		int countBefore = commentsDAO.selectAll().size();
		commentsDAO.delete(2L);
		assertEquals(commentsDAO.selectAll().size(), countBefore - 1);
	}
	

	@Test
	public void topNewsCount() throws Exception {
		assertEquals(commentsDAO.topNews().size(), 7);
	}
	
	@Test
	public void deleteByNewsId() throws Exception {
		commentsDAO.deleteByNewsId(1L);
		assertEquals(commentsDAO.selectAll().size(), 4);
	}
}