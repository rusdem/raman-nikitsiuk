package dbtest.impl;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.lab.nikitsuik.dao.NewsDAO;
import com.epam.lab.nikitsuik.dao.impl.NewsDAOImpl;
import com.epam.lab.nikitsuik.entity.Author;
import com.epam.lab.nikitsuik.entity.News;
import com.epam.lab.nikitsuik.entity.SearchCriteria;
import com.epam.lab.nikitsuik.entity.Tag;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import dbtest.DAOTest;

@DatabaseSetup("/data-set.xml")
public class NewsDAOImplTest extends DAOTest {
	@Autowired
	NewsDAOImpl newsDAO;
	@Test
	public void countNews() throws Exception {
		assertEquals(new Long(newsDAO.selectAll().size()), newsDAO.countNews());
	}
	
	@Test 
	public void search() throws Exception {
		Author author = new Author();
		author.setAuthorName("John");
		author.setId(1L);
		List<Tag> lst = new ArrayList<Tag>();
		Tag tag = new Tag();
		tag.setId(1L);
		tag.setTagName("tag1");
		lst.add(tag);
		assertEquals(newsDAO.search(new SearchCriteria(author, lst)).size(), 1);
	}

	@Test
	public void addNews() throws Exception {
		News news = new News();
		news.setId(6L);
		news.setFullText("FText");
		news.setShortText("SText");
		news.setTitle("TText");
		news.setCreationDate(new Timestamp(2L));
		news.setModificationDate(new Date(2L));
		int countBefore = newsDAO.selectAll().size();
		newsDAO.add(news);
		assertEquals(newsDAO.selectAll().size(), countBefore + 1);
	}


	@Test
	public void readNews() throws Exception {
		assertEquals(newsDAO.select(2L).getId(), new Long(2L));
	}

	@Test
	public void updateNews() throws Exception {
		News news = new News();
		news.setId(6L);
		news.setFullText("SText");
		news.setShortText("SText");
		news.setTitle("SText");
		news.setCreationDate(new Timestamp(2L));
		news.setModificationDate(new Date(2L));
		newsDAO.update(news);
		assertEquals(newsDAO.select(6L).getTitle(), "SText");
	}


	@Test
	public void deleteNews() throws Exception {
		int countBefore = newsDAO.selectAll().size();
		newsDAO.delete(2L);
		assertEquals(newsDAO.selectAll().size(), countBefore - 1);
	}
}