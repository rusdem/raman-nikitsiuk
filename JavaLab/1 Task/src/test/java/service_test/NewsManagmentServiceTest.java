package service_test;


import com.epam.lab.nikitsuik.dao.AuthorDAO;
import com.epam.lab.nikitsuik.dao.CommentsDAO;
import com.epam.lab.nikitsuik.dao.NewsDAO;
import com.epam.lab.nikitsuik.dao.TagDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.*;
import com.epam.lab.nikitsuik.service.*;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.service.impl.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class NewsManagmentServiceTest {
    @Mock
    private AuthorDAO authorDAO;
    @Mock
    private CommentsDAO commentDAO;
    @Mock
    private NewsDAO newsDAO;
    @Mock
    private TagDAO tagDAO;

    private NewsManager newsManagemer;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);

        AuthorService authorService = new AuthorServiceImpl();
        authorService.setAuthorDAO(authorDAO);

        CommentsService commentService = new CommentsServiceImpl();
        commentService.setCommentsDAO(commentDAO);

        NewsService newsService = new NewsServiceImpl();
        newsService.setNewsDAO(newsDAO);

        TagService tagService = new TagServiceImpl();
        tagService.setTagDAO(tagDAO);


        newsManagemer = new NewsManagerImpl();

        newsManagemer.setAuthorService(authorService);
        newsManagemer.setCommentsService(commentService);
        newsManagemer.setNewsService(newsService);
        newsManagemer.setTagService(tagService);
    }

    @Test
    public void addNews() throws DAOException, ServiceException {
        List<Tag> list = new ArrayList<Tag>();
        Tag tag = mock(Tag.class);
        list.add(tag);
        List<Long> lst = new ArrayList<Long>();
        lst.add(3L);
        News news = mock(News.class);
        Author author = mock(Author.class);
        when(newsDAO.add(any(News.class))).thenReturn(1L);
        when(authorDAO.add(any(Author.class))).thenReturn(2L);
        when(tagDAO.addTags(anyList())).thenReturn(anyList());
        newsManagemer.addNews(news,author,list);

        verify(newsDAO).add(any(News.class));
        verify(authorDAO).add(any(Author.class));
        verify(tagDAO).addTags(anyList());
        verify(newsDAO).addAuthorLink(anyLong(),anyLong());
        verify(newsDAO).addTagLinks(anyLong(),anyList());
    }

    @Test
    public void deleteNews() throws DAOException, ServiceException {

        newsManagemer.deleteNews(anyLong());

        verify(commentDAO).deleteByNewsId(anyLong());
        verify(newsDAO).delete(anyLong());
    }

    @Test
    public void getNews() throws DAOException, ServiceException {
        newsManagemer.viewNews(anyLong());
        verify(newsDAO).select(anyLong());
    }

    @Test
    public void addComments() throws DAOException, ServiceException {

        List<Comments> list = new ArrayList<Comments>();
        list.add(any(Comments.class));
        newsManagemer.addComments(list);

        verify(commentDAO).add(any(Comments.class));;
    }

    @Test
    public void searchNews() throws DAOException, ServiceException {
        newsManagemer.search(any(SearchCriteria.class));
        verify(newsDAO).search(any(SearchCriteria.class));
    }

    @Test
    public void addNewsAuthor() throws DAOException, ServiceException {
        Author author = mock(Author.class);

        newsManagemer.addNewsAuthor(anyLong(),author);

        verify(newsDAO).addAuthorLink(anyLong(),anyLong());
        verify(authorDAO).add(author);
    }
}