package service_test;


import com.epam.lab.nikitsuik.dao.CommentsDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Comments;
import com.epam.lab.nikitsuik.service.CommentsService;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.service.impl.CommentsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class CommentsServiceTest {
    @Mock
    private CommentsDAO commentDAO;
    private CommentsService commentService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        commentService = new CommentsServiceImpl();
        commentService.setCommentsDAO(commentDAO);
    }

    @Test
    public void getComments() throws DAOException, ServiceException {

        Comments mockedComments = new Comments();
        mockedComments.setCommentText("comment text");

        when(commentDAO.select(anyLong())).thenReturn(mockedComments);

        Comments comment = commentService.select(anyLong());

        assertEquals("comment text", comment.getCommentText());
        verify(commentDAO).select(anyLong());
    }

    @Test
    public void createComments() throws DAOException, ServiceException {

        Comments mockedComments = new Comments();
        mockedComments.setCommentText("comment text");

        when(commentDAO.add(mockedComments)).thenReturn(anyLong());

        Long id = commentService.add(mockedComments);

        assertEquals(Long.class, id.getClass());

        verify(commentDAO).add(mockedComments);

    }

    @Test
    public void updateComments() throws DAOException {

        Comments mockedComments = new Comments();
        mockedComments.setCommentText("comment text");

        DAOException e = new DAOException();
        doThrow(e).when(commentDAO).update(mockedComments);


        boolean isPassed = false;

        try {
            commentService.update(mockedComments);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(commentDAO).update(mockedComments);

    }

    @Test
    public void deleteByNewsId() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(commentDAO).deleteByNewsId(anyLong());

        boolean isPassed = false;

        try {
            commentService.deleteByNewsId(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(commentDAO).deleteByNewsId(anyLong());
    }
}