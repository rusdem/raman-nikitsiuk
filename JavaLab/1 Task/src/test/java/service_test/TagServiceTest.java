package service_test;

import com.epam.lab.nikitsuik.dao.TagDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Tag;
import com.epam.lab.nikitsuik.service.TagService;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.service.impl.TagServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class TagServiceTest {
    @Mock
    private TagDAO tagDAO;
    private TagService tagService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        tagService = new TagServiceImpl();
        tagService.setTagDAO(tagDAO);
    }

    @Test
    public void getTag() throws DAOException, ServiceException {

        Tag mockedTag = new Tag();
        mockedTag.setTagName("tag");

        when(tagDAO.select(anyLong())).thenReturn(mockedTag);

        Tag tag = tagService.select(anyLong());

        assertEquals("tag", tag.getTagName());
        verify(tagDAO).select(anyLong());
    }

    @Test
    public void createTag() throws DAOException, ServiceException {

        Tag mockedTag = new Tag();
        mockedTag.setTagName("tag");

        when(tagDAO.add(mockedTag)).thenReturn(anyLong());

        Long id = tagService.add(mockedTag);

        assertEquals(Long.class, id.getClass());
        verify(tagDAO).add(mockedTag);
    }

    @Test
    public void updateTag() throws DAOException {

        Tag mockedTag = new Tag();
        mockedTag.setTagName("tag");

        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).update(mockedTag);

        boolean isPassed = false;

        try {
            tagService.update(mockedTag);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).update(mockedTag);
    }

    @Test
    public void deleteTag() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).delete(anyLong());

        boolean isPassed = false;

        try {
            tagService.delete(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).delete(anyLong());
    }

}