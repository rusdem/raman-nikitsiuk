package service_test;

import com.epam.lab.nikitsuik.dao.AuthorDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Author;
import com.epam.lab.nikitsuik.service.AuthorService;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.service.impl.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class AuthorServiceTest
{
    @Mock
    private AuthorDAO authorDAO;
    private AuthorService authorService;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        authorService = new AuthorServiceImpl();
        authorService.setAuthorDAO(authorDAO);
    }

    @Test
    public void getAuthor() throws DAOException, ServiceException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorName("SOMEBODY");

        when( authorDAO.select(anyLong())).thenReturn(mockedAuthor);

        Author author = authorService.select(anyLong());

        assertEquals("SOMEBODY", author.getAuthorName());
        verify(authorDAO).select(anyLong());

    }
    @Test
    public void createAuthor() throws DAOException, ServiceException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorName("SOMEBODY");

        when(authorDAO.add(mockedAuthor)).thenReturn(anyLong());

        Long id = authorService.add(mockedAuthor);

        assertEquals(Long.class, id.getClass());
        verify(authorDAO).add(mockedAuthor);
    }

    @Test
    public void updateAuthor() throws DAOException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorName("SOMEBODY");

        DAOException e = new DAOException();
        doThrow(e).when(authorDAO).update(mockedAuthor);


        boolean isPassed = false;

        try {
            authorService.update(mockedAuthor);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(authorDAO).update(mockedAuthor);
    }

    @Test
    public void deleteAuthor() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(authorDAO).delete(anyLong());

        boolean isPassed = false;

        try {
            authorService.delete(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(authorDAO).delete(anyLong());
    }

    @Test
    public void getAllAuthors() throws DAOException, ServiceException {

        List<Author> list = new ArrayList<Author>();
        when( authorDAO.selectAll()).thenReturn((ArrayList<Author>) list);

        List<Author> resultList = authorService.selectAll();

        assertEquals(resultList, list);
        verify(authorDAO).selectAll();
    }
}