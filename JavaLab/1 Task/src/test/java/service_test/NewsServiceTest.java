package service_test;


import com.epam.lab.nikitsuik.dao.NewsDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.News;
import com.epam.lab.nikitsuik.entity.SearchCriteria;
import com.epam.lab.nikitsuik.service.NewsService;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.service.impl.NewsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class NewsServiceTest {
    @Mock
    private NewsDAO newsDAO;
    private NewsService newsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        newsService = new NewsServiceImpl();
        newsService.setNewsDAO(newsDAO);
    }

    @Test
    public void getNews() throws DAOException, ServiceException {

        News mockedNews = new News();
        mockedNews.setTitle("title");
        mockedNews.setShortText("short text");
        mockedNews.setFullText("full text");

        when(newsDAO.select(anyLong())).thenReturn(mockedNews);

        News news = newsService.select(anyLong());


        assertEquals("title", news.getTitle());
        assertEquals("short text", news.getShortText());
        assertEquals("full text", news.getFullText());
        verify(newsDAO).select(anyLong());
    }

    @Test
    public void createNews() throws DAOException, ServiceException {

        News mockedNews = new News();
        mockedNews.setTitle("title");
        mockedNews.setShortText("short text");
        mockedNews.setFullText("full text");

        when(newsDAO.add(mockedNews)).thenReturn(anyLong());

        Long id = newsService.add(mockedNews);

        assertEquals(Long.class, id.getClass());

        verify(newsDAO).add(mockedNews);
    }

    @Test
    public void updateNews() throws DAOException {

        News mockedNews = new News();
        mockedNews.setTitle("title");
        mockedNews.setShortText("short text");
        mockedNews.setFullText("full text");

        DAOException e = new DAOException();
        doThrow(e).when(newsDAO).update(mockedNews);

        boolean isPassed = false;

        try {
            newsService.update(mockedNews);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(newsDAO).update(mockedNews);

    }


    @Test
    public void deleteNews() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(newsDAO).delete(anyLong());

        boolean isPassed = false;

        try {
            newsService.delete(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(newsDAO).delete(anyLong());
    }

    @Test
    public void searchNews() throws DAOException, ServiceException {

        List<News> list = new ArrayList<News>();
        SearchCriteria searchCriteria = new SearchCriteria();
        DAOException e = new DAOException();
        doThrow(e).when(newsDAO).search(searchCriteria);
        boolean isPassed = false;
        try {
            newsService.search(searchCriteria);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
    }
}