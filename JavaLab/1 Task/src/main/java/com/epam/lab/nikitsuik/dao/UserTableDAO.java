package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.entity.UserTable;

public interface UserTableDAO extends TemplateDAO<UserTable> {
}
