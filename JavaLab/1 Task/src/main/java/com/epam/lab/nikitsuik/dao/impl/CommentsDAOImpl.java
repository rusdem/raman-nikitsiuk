package com.epam.lab.nikitsuik.dao.impl;

import com.epam.lab.nikitsuik.dao.CommentsDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Comments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentsDAOImpl implements CommentsDAO {
    @Autowired
    private DriverManagerDataSource dataSource = null;

    public void setDataSource(DriverManagerDataSource dataSource)  {
        this.dataSource = dataSource;
    }

    private static final String ADD = "INSERT INTO comments (comments_id, news_id, comment_text, creation_date) VALUES (COMMENTS_AUTOINCREMENT.nextval,?,?,?)";
    private static final String DELETE = "DELETE FROM comments WHERE comment_id=?";
    private static final String SELECT = "SELECT comment_id, news_id,comment_text, creation_date FROM comments WHERE comment_id=?";
    private static final String SELECT_ALL = "SELECT comment_id, news_id, comment_text, creation_date FROM comments ORDER BY comment_id";
    private static final String UPDATE = "UPDATE comments SET news_id=?, comment_text=?, creation_date=? WHERE comment_id=?";
    private static final String DELETE_BY_NEWS_ID = "DELETE FROM comments WHERE news_id=?";
    private static final String TOP_NEWS = "SELECT NEWS_ID FROM COMMENTS GROUP BY NEWS_ID ORDER BY COUNT(NEWS_ID) DESC";

    public Long add(Comments obj) {
        Long resultId = null;
        boolean resultFlag = true;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            int generatedColomns[] = {1};
            stmt = conn.prepareStatement(ADD, generatedColomns);
            stmt.setLong(1, obj.getNewsId());
            stmt.setString(2, obj.getCommentText());
            stmt.setTimestamp(3, obj.getCreationDate());
            stmt.executeUpdate();
            ResultSet resultSet = stmt.getGeneratedKeys();
            resultSet.next();
            resultId = resultSet.getLong(1);
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return resultId;
    }

    public void delete(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    public Comments select(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Comments comments = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT);
            stmt.setLong(1, id);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    comments = new Comments(resultSet.getLong(1), resultSet.getLong(2), resultSet.getString(3), resultSet.getTimestamp(4));
                }
            }
        } catch (SQLException e) {

            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {

                throw new DAOException();
            }
        }
        return comments;
    }

    public ArrayList<Comments> selectAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<Comments> arrayList = new ArrayList<Comments>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL)) != null) {
                while (resultSet.next()) {
                    arrayList.add(new Comments(resultSet.getLong(1), resultSet.getLong(2), resultSet.getString(3), resultSet.getTimestamp(4)));
                }
            }
        } catch (SQLException e) {

            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {

                throw new DAOException();
            }
        }
        return arrayList;
    }

    public void update(Comments obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(UPDATE);
            stmt.setLong(1, obj.getNewsId());
            stmt.setString(2, obj.getCommentText());
            stmt.setTimestamp(3, obj.getCreationDate());
            stmt.setLong(4, obj.getId());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    public void deleteByNewsId(Long newsId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_BY_NEWS_ID);
            stmt.setLong(1, newsId);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    public List<Long> topNews() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<Long> arrayList = new ArrayList<Long>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL)) != null) {
                while (resultSet.next()) {
                    arrayList.add(resultSet.getLong(1));
                }
            }
        } catch (SQLException e) {

            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {

                throw new DAOException();
            }
        }
        return arrayList;
    }
}
