package com.epam.lab.nikitsuik.dao.impl;

import com.epam.lab.nikitsuik.dao.AuthorDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;

public class AuthorDAOImpl implements AuthorDAO {
    @Autowired
    private DriverManagerDataSource dataSource;

    public void setDataSource(DriverManagerDataSource dataSource)  {
        this.dataSource = dataSource;
    }
    
    public DriverManagerDataSource getDataSource() {
		return dataSource;
	}

	private static final String ADD = "INSERT INTO author (author_id, author_name, expired) VALUES (AUTHOR_AUTOINCREMENT.nextval,?,?)";
    private static final String DELETE = "DELETE FROM author WHERE author_id=?";
    private static final String SELECT = "SELECT author_id, author_name, expired FROM author WHERE author_id=?";
    private static final String SELECT_ALL = "SELECT author_id, author_name, expired FROM author ORDER BY author_id";
    private static final String UPDATE = "UPDATE author SET author_name=?, expired=? WHERE author_id=?";
    private static final String DELETE_AUTHOR_LINK = "DELETE FROM news_author WHERE author_id=?";

    public Long add(Author obj) {
        Long resultId = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            int generatedColomns[] = {1};
            stmt = conn.prepareStatement(ADD, generatedColomns);
            stmt.setString(1, obj.getAuthorName());
            stmt.setTimestamp(2, obj.getExpired());
            stmt.executeUpdate();
            ResultSet resultSet = stmt.getGeneratedKeys();
            resultSet.next();
            resultId = resultSet.getLong(1);
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return resultId;
    }

	public void delete(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_AUTHOR_LINK);
            stmt.setLong(1, id);
            stmt.execute();
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    public Author select(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Author author = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT);
            stmt.setLong(1, id);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    author = new Author(resultSet.getLong(1), resultSet.getString(2), resultSet.getTimestamp(3));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {

                throw new DAOException();
            }
        }
        return author;
    }

    public ArrayList<Author> selectAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<Author> arrayList = new ArrayList<Author>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL)) != null) {
                while (resultSet.next()) {
                    arrayList.add(new Author(resultSet.getLong(1), resultSet.getString(2), resultSet.getTimestamp(3)));
                }
            }
        } catch (SQLException e) {

            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {

                throw new DAOException();
            }
        }
        return arrayList;
    }

    public void update(Author obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(UPDATE);
            stmt.setString(1, obj.getAuthorName());
            stmt.setTimestamp(2, obj.getExpired());
            stmt.setLong(3, obj.getId());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

}
