package com.epam.lab.nikitsuik.dao.impl;

import com.epam.lab.nikitsuik.dao.RolesDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;

public class RolesDAOImpl implements RolesDAO {
    @Autowired
    private DriverManagerDataSource dataSource = null;

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final String ADD = "INSERT INTO roles (user_id, role_name) VALUES (?,?)";
    private static final String DELETE = "DELETE FROM roles WHERE user_id=?";
    private static final String SELECT = "SELECT user_id, role_name FROM roles WHERE user_id=?";
    private static final String SELECT_ALL = "SELECT user_id, role_name FROM roles ORDER BY user_id";
    private static final String UPDATE = "UPDATE roles SET role_name=? WHERE user_id=?";

    
    public Long add(Roles obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(ADD);
            stmt.setLong(1, obj.getId());
            stmt.setString(2, obj.getRoleName());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return obj.getId();
    }

    public void delete(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public Roles select(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Roles roles = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT);
            stmt.setLong(1, id);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    roles = new Roles(resultSet.getLong(1), resultSet.getString(2));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return roles;
    }

    
    public ArrayList<Roles> selectAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<Roles> arrayList = new ArrayList<Roles>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL)) != null) {
                while (resultSet.next()) {
                    arrayList.add(new Roles(resultSet.getLong(1), resultSet.getString(2)));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return arrayList;
    }

    
    public void update(Roles obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(UPDATE);
            stmt.setString(1, obj.getRoleName());
            stmt.setLong(2, obj.getId());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }
}
