package com.epam.lab.nikitsuik.service.impl;

import com.epam.lab.nikitsuik.dao.NewsDAO;
import com.epam.lab.nikitsuik.entity.News;
import com.epam.lab.nikitsuik.entity.SearchCriteria;
import com.epam.lab.nikitsuik.service.AuthorService;
import com.epam.lab.nikitsuik.service.CommentsService;
import com.epam.lab.nikitsuik.service.NewsService;
import com.epam.lab.nikitsuik.service.TagService;
import com.epam.lab.nikitsuik.service.exception.ServiceException;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class NewsServiceImpl implements NewsService {
	private NewsDAO newsDAO = null;

	public void setNewsDAO(NewsDAO newsDAO){
        this.newsDAO = newsDAO;
    }

	public Long add(News obj) throws ServiceException {
		try {
			return newsDAO.add(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void delete(Long id) throws ServiceException {
		try {
			newsDAO.delete(id);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public News select(Long id) throws ServiceException {
		try {
			return newsDAO.select(id);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public ArrayList<News> selectAll() throws ServiceException {
		try {
			return newsDAO.selectAll();
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void update(News obj) throws ServiceException {
		try {
			newsDAO.update(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public Long countNews() throws ServiceException {
		try {
			return newsDAO.countNews();
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void addTagLink(Long newsId, Long tagId) throws ServiceException {
		try {
			newsDAO.addTagLink(newsId, tagId);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void deleteTagLink(Long newsId, Long tagId) throws ServiceException {
		try {
			newsDAO.deleteTagLink(newsId, tagId);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void addAuthorLink(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.addAuthorLink(newsId, authorId);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void deleteAuthorLink(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.deleteAuthorLink(newsId, authorId);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void addTagLinks(Long newsId, List<Long> tags) throws ServiceException {
		try {
			newsDAO.addTagLinks(newsId, tags);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public List<News> getTopNews(List<Long> newsId) throws ServiceException {
		try {
			return newsDAO.getTopNews(newsId);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public List<News> search(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.search(searchCriteria);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

}
