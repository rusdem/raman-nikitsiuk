package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Comments;

import java.util.List;

public interface CommentsDAO extends TemplateDAO<Comments> {
    /**
     * Delete all comments by news id.
     * @param newsId
     * @throws DAOException
     */
    public void deleteByNewsId(Long newsId) throws DAOException;

    /**
     * Gives list of news sorted by count of comments
     * @return list of news id
     * @throws DAOException
     */
    public List<Long> topNews() throws DAOException;
}
