package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.dao.exception.DAOException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.ArrayList;

/**
 * CRUD operations interface
 * @param <T> entity object
 */
public interface TemplateDAO<T> {
    public Long add(T obj) throws DAOException;
    public void delete(Long id) throws DAOException;
    public T select(Long id) throws DAOException;
    public ArrayList<T> selectAll() throws DAOException;
    public void update(T obj) throws DAOException;
    public void setDataSource(DriverManagerDataSource dataSource);
}
