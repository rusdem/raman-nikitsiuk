package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.entity.Roles;

public interface RolesDAO extends TemplateDAO<Roles> {
}
