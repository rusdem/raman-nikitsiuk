package com.epam.lab.nikitsuik.service.impl;

import com.epam.lab.nikitsuik.entity.*;
import com.epam.lab.nikitsuik.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class NewsManagerImpl implements NewsManager {
    @Autowired
    private AuthorService authorService = null;
    @Autowired
    private CommentsService commentsService = null;
    @Autowired
    private NewsService newsService = null;
    @Autowired
    private TagService tagService = null;

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public void setCommentsService(CommentsService commentsService) {
        this.commentsService = commentsService;
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    
    public void addNews(News obj) {
        newsService.add(obj);
    }

    
    public void editNews(News obj) {
        newsService.update(obj);
    }

    
    public void deleteNews(Long newsId) {
        commentsService.deleteByNewsId(newsId);
        newsService.delete(newsId);
    }

    
    @Transactional(readOnly = true)
    public List<News> viewNewsList() {
        return newsService.selectAll();
    }

    
    @Transactional(readOnly = true)
    public News viewNews(Long newsId) {
        return newsService.select(newsId);
    }

    
    public void addNewsAuthor(Long newsId, Author author) {
        author.setId(authorService.add(author));
        newsService.addAuthorLink(newsId, author.getId());
    }

    
    public List<News> search(SearchCriteria searchCriteria) {
        return newsService.search(searchCriteria);
    }

    
    public void addTagsToNews(Long newsId, List<Tag> tags) {
        Iterator<Tag> iterator = tags.iterator();
        List<Long> list = new ArrayList<Long>(tags.size());
        while (iterator.hasNext()) {
            list.add(iterator.next().getId());
        }
        newsService.addTagLinks(newsId,list);
    }

    
    public void addComments(List<Comments> commentsList) {
        Iterator<Comments> iterator = commentsList.iterator();
        while (iterator.hasNext()){
            commentsService.add(iterator.next());
        }
    }

    
    public void deleteComments(List<Comments> commentsList) {
        Iterator<Comments> iterator = commentsList.iterator();
        while (iterator.hasNext()){
            commentsService.delete(iterator.next().getId());
        }
    }

    
    @Transactional(readOnly = true)
    public List<News> viewSortedNewsList() {
        return newsService.getTopNews(commentsService.topNews());
    }

    
    public void addNews(News news, Author author, List<Tag> tags) {
        news.setId(newsService.add(news));
        newsService.addAuthorLink(news.getId(),authorService.add(author));
        newsService.addTagLinks(news.getId(),tagService.addTags(tags));
    }

    
    @Transactional(readOnly = true)
    public Long countNews() {
        return newsService.countNews();
    }


}
