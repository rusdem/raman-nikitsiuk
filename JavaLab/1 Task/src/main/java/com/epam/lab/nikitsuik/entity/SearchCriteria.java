package com.epam.lab.nikitsuik.entity;

import java.io.Serializable;
import java.util.List;

public class SearchCriteria implements Serializable {
    private List<Tag> tags = null;
    private Author author = null;

    public SearchCriteria() {
    }

    public SearchCriteria(Author author, List<Tag> tags) {
        this.author = author;
        this.tags = tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Author getAuthor() {
        return author;
    }
}
