package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.dao.CommentsDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Comments;

import java.util.ArrayList;
import java.util.List;

public interface CommentsService {
    public void setCommentsDAO(CommentsDAO commentsDAO);

    public Long add(Comments obj) throws ServiceException;

    public void delete(Long id) throws ServiceException;

    public Comments select(Long id) throws ServiceException;

    public ArrayList<Comments> selectAll() throws ServiceException;

    public void update(Comments obj) throws ServiceException;

    /**
     * Delete all comments by news id.
     * @param newsId
     * @throws ServiceException
     */
    public void deleteByNewsId(Long newsId) throws ServiceException;

    /**
     * Gives list of news sorted by count of comments
     * @return list of news id
     * @throws ServiceException
     */
    public List<Long> topNews() throws ServiceException;
}
