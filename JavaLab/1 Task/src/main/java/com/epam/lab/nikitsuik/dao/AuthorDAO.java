package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.entity.Author;

public interface AuthorDAO extends TemplateDAO<Author> {
	
}
