package com.epam.lab.nikitsuik.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comments implements Serializable {
    private Long id;
    private Long newsId;
    private String commentText;
    private Timestamp creationDate;

    public Comments() {
    }

    public Comments(Long newsId, String commentText, Timestamp creationDate) {
        this.newsId = newsId;
        this.commentText = commentText;
        this.creationDate = creationDate;
    }

    public Comments(Long id, Long newsId, String commentText, Timestamp creationDate) {
        this.id = id;
        this.newsId = newsId;
        this.commentText = commentText;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}
