package com.epam.lab.nikitsuik.service.impl;

import com.epam.lab.nikitsuik.dao.AuthorDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Author;
import com.epam.lab.nikitsuik.service.AuthorService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class AuthorServiceImpl implements AuthorService {
	private AuthorDAO authorDAO = null;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public Long add(Author obj) throws ServiceException {
		try {
			return authorDAO.add(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void delete(Long id) throws ServiceException {
		try {
			authorDAO.delete(id);
		} catch (Exception e){
			throw new ServiceException();
		}

	}

	@Transactional(readOnly = true)
	public Author select(Long id) throws ServiceException {
		try {
			return authorDAO.select(id);
		} catch (Exception e){
			throw new ServiceException();
		}

	}

	@Transactional(readOnly = true)
	public ArrayList<Author> selectAll() throws ServiceException {
		try {
			return authorDAO.selectAll();
		} catch (Exception e){
			throw new ServiceException();
		}

	}

	public void update(Author obj) throws ServiceException {
		try {
			authorDAO.update(obj);
		} catch (Exception e){
			throw new ServiceException();
		}
	}
}
