package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.dao.AuthorDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Author;

import java.util.ArrayList;

public interface AuthorService {
    public void setAuthorDAO(AuthorDAO authorDAO);

    public Long add(Author obj) throws ServiceException;

    public void delete(Long id) throws ServiceException;

    public Author select(Long id) throws ServiceException;

    public ArrayList<Author> selectAll() throws ServiceException;

    public void update(Author obj) throws ServiceException;
}
