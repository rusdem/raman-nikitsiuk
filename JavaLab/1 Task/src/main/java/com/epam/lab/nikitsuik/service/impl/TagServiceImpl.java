package com.epam.lab.nikitsuik.service.impl;

import com.epam.lab.nikitsuik.dao.TagDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Tag;
import com.epam.lab.nikitsuik.service.TagService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class TagServiceImpl implements TagService {
	private TagDAO tagDAO = null;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	public Long add(Tag obj) throws ServiceException {
		try {
			return tagDAO.add(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void delete(Long id) throws ServiceException {
		try {
			tagDAO.delete(id);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public Tag select(Long id) throws ServiceException {
		try {
			return tagDAO.select(id);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public ArrayList<Tag> selectAll() throws ServiceException {
		try {
			return tagDAO.selectAll();
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void update(Tag obj) throws ServiceException {
		try {
			tagDAO.update(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public List<Long> addTags(List<Tag> tags) {
		try {
			return tagDAO.addTags(tags);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}
}
