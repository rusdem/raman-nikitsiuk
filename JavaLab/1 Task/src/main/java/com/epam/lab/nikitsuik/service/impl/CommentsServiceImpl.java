package com.epam.lab.nikitsuik.service.impl;

import com.epam.lab.nikitsuik.dao.CommentsDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Comments;
import com.epam.lab.nikitsuik.service.CommentsService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class CommentsServiceImpl implements CommentsService {
	private CommentsDAO commentsDAO = null;

	public void setCommentsDAO(CommentsDAO commentsDAO) {
		this.commentsDAO = commentsDAO;
	}

	public Long add(Comments obj) throws ServiceException {
		try {
			return commentsDAO.add(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void delete(Long id) throws ServiceException {
		try {
			commentsDAO.delete(id);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public Comments select(Long id) throws ServiceException {
		try {
			return commentsDAO.select(id);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	@Transactional(readOnly = true)
	public ArrayList<Comments> selectAll() throws ServiceException {
		try {
			return commentsDAO.selectAll();
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void update(Comments obj) throws ServiceException {
		try {
			commentsDAO.update(obj);
		} catch (Exception e) {
			throw new ServiceException();
		}
	}

	public void deleteByNewsId(Long newsId) {
		try {
			commentsDAO.deleteByNewsId(newsId);
		} catch (Exception e) {
			throw new ServiceException();
		}

	}

	public List<Long> topNews() {
		try {
			return commentsDAO.topNews();
		} catch (Exception e) {
			throw new ServiceException();
		}
	}
}
