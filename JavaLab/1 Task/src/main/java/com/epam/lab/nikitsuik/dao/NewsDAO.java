package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.News;
import com.epam.lab.nikitsuik.entity.SearchCriteria;

import java.util.List;

public interface NewsDAO extends TemplateDAO<News> {
    public Long countNews() throws DAOException;

    /**
     * Make records in table News_Tag
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    public void addTagLink(Long newsId, Long tagId) throws DAOException;
    public void deleteTagLink(Long newsId, Long tagId) throws DAOException;
    /**
     * Make records in table News_Author
     * @param newsId
     * @param authorId
     * @throws DAOException
     */
    public void addAuthorLink(Long newsId, Long authorId) throws DAOException;
    public void deleteAuthorLink(Long newsId, Long authorId) throws DAOException;
    public void addTagLinks(Long newsId, List<Long> tags) throws DAOException;

    /**
     * Search news by tags and author.
     * @param searchCriteria entity with Tags and Author info
     * @return List of news that linked with tags and author in searchCriteria
     * @throws DAOException
     */
    public List<News> search(SearchCriteria searchCriteria) throws DAOException;
    public List<News> getTopNews(List<Long> newsId) throws DAOException;
}
