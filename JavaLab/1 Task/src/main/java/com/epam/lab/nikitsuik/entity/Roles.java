package com.epam.lab.nikitsuik.entity;

import java.io.Serializable;

public class Roles implements Serializable {
    private Long id;
    private String roleName;

    public Roles() {
    }

    public Roles(Long id, String roleName) {
        this.roleName = roleName;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
