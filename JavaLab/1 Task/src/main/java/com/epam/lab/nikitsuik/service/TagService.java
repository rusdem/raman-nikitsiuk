package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.dao.TagDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Tag;

import java.util.ArrayList;
import java.util.List;

public interface TagService {
    public void setTagDAO(TagDAO tagDAO);

    public Long add(Tag obj) throws ServiceException;

    public void delete(Long id) throws ServiceException;

    public Tag select(Long id) throws ServiceException;

    public List<Tag> selectAll() throws ServiceException;

    public void update(Tag obj) throws ServiceException;

    /**
     * Add group of tags
     * @param tags
     * @return list of generated id in database to added tags
     * @throws ServiceException
     */
    public List<Long> addTags(List<Tag> tags) throws ServiceException;
}
