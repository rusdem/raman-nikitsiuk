package com.epam.lab.nikitsuik.entity;


import java.io.Serializable;
import java.sql.Timestamp;

public class Author implements Serializable{
    private Long id;
    private String authorName;
    private Timestamp expired;


    public Author() {
    }

    public Author(String authorName, Timestamp expired) {
        this.authorName = authorName;
        this.expired = expired;
    }

    public Author(Long id, String authorName, Timestamp expired) {
        this.authorName = authorName;
        this.expired = expired;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }
}
