package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.entity.*;

import java.util.List;

/**
 * Common service
 */
public interface NewsManager {
    public void setAuthorService(AuthorService authorService);

    public void setCommentsService(CommentsService commentsService);

    public void setNewsService(NewsService newsService);

    public void setTagService(TagService tagService);

    public void addNews(News obj);

    public void editNews(News obj);

    public void deleteNews(Long newsId);

    public List<News> viewNewsList();

    public News viewNews(Long newsId);

    public void addNewsAuthor(Long newsId, Author author);

    /**
     * Search news by tags and author.
     * @param searchCriteria entity with Tags and Author info
     * @return List of news that linked with tags and author in searchCriteria
     */
    public List<News> search(SearchCriteria searchCriteria);

    public void addTagsToNews(Long newsId, List<Tag> tags);

    public void addComments(List<Comments> commentsList);

    public void deleteComments(List<Comments> commentsList);

    public List<News> viewSortedNewsList();

    /**
     * Add new Author record, group of Tags, and News that linked with author and tags.
     * @param news
     * @param author
     * @param tags
     */
    public void addNews(News news, Author author, List<Tag> tags);

    public Long countNews();
}
