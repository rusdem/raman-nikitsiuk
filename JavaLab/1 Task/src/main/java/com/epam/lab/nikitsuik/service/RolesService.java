package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.dao.RolesDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.Roles;

import java.util.ArrayList;

public interface RolesService {
    public void setRolesDAO(RolesDAO rolesDAO);

    public Long add(Roles obj) throws ServiceException;

    public void delete(Long id) throws ServiceException;

    public Roles select(Long id) throws ServiceException;

    public ArrayList<Roles> selectAll() throws ServiceException;

    public void update(Roles obj) throws ServiceException;
}
