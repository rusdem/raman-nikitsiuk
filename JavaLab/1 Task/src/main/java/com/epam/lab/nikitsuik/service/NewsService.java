package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.dao.NewsDAO;
import com.epam.lab.nikitsuik.entity.News;
import com.epam.lab.nikitsuik.entity.SearchCriteria;

import java.util.ArrayList;
import java.util.List;

public interface NewsService {
    public void setNewsDAO(NewsDAO newsDAO);

    public Long add(News obj);

    public void delete(Long id);

    public News select(Long id);

    public ArrayList<News> selectAll();

    public void update(News obj);

    public Long countNews();

    public void addTagLink(Long newsId, Long tagId);

    public void deleteTagLink(Long newsId, Long tagId);

    public void addAuthorLink(Long newsId, Long authorId);

    public void deleteAuthorLink(Long newsId, Long authorId);

    public void addTagLinks(Long newsId, List<Long> tags);

    public List<News> getTopNews(List<Long> newsId);

    public List<News> search(SearchCriteria searchCriteria);
}
