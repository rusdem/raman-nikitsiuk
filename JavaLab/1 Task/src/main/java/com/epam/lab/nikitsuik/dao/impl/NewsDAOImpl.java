package com.epam.lab.nikitsuik.dao.impl;

import com.epam.lab.nikitsuik.dao.NewsDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.News;
import com.epam.lab.nikitsuik.entity.SearchCriteria;
import com.epam.lab.nikitsuik.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NewsDAOImpl implements NewsDAO {
    @Autowired
    private DriverManagerDataSource dataSource = null;
    public void setDataSource(DriverManagerDataSource dataSource)  {
        this.dataSource = dataSource;
    }

    private static final String NEWS_COUNT = "SELECT COUNT(*) FROM news;";
    private static final String ADD_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_AUTOINCREMENT.nextval,?,?,?,?,?)";
    private static final String DELETE_NEWS = "DELETE FROM news WHERE news_id=?";
    private static final String DELETE_AUTHOR_LINKS = "DELETE FROM news_author WHERE news_id=?";
    private static final String DELETE_TAG_LINKS = "DELETE FROM news_tag WHERE news_id=?";
    private static final String SELECT_NEWS = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news WHERE news_id=?";
    private static final String SELECT_ALL_NEWS = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news ORDER BY news_id";
    private static final String UPDATE_NEWS = "UPDATE news SET title=?, short_text=?, full_text=?, creation_date=?, modification_date=? WHERE news_id=?";
    private static final String ADD_TAG_TO_NEWS = "INSERT INTO news_tag (news_id, tag_id) VALUES (?,?)";
    private static final String DELETE_TAG_FROM_NEWS = "DELETE FROM news_tag WHERE news_id=? AND tag_id=?";
    private static final String ADD_AUTHOR_TO_NEWS = "INSERT INTO news_author (news_id, author_id) VALUES (?,?)";
    private static final String DELETE_AUTHOR_FROM_NEWS = "DELETE FROM news_author WHERE news_id=? AND author_id=?";


    
    public Long add(News obj) {
        Long resultId = null;
        boolean resultFlag = true;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            int generatedColomns[] = {1};
            stmt = conn.prepareStatement(ADD_NEWS, generatedColomns);
            stmt.setString(1, obj.getTitle());
            stmt.setString(2, obj.getShortText());
            stmt.setString(3, obj.getFullText());
            stmt.setTimestamp(4, obj.getCreationDate());
            stmt.setDate(5, obj.getModificationDate());
            stmt.executeUpdate();
            ResultSet resultSet = stmt.getGeneratedKeys();
            resultSet.next();
            resultId = resultSet.getLong(1);
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return resultId;
    }

    
    public void delete(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_TAG_LINKS);
            stmt.setLong(1, id);
            stmt.execute();
            stmt = conn.prepareStatement(DELETE_AUTHOR_LINKS);
            stmt.setLong(1, id);
            stmt.execute();
            stmt = conn.prepareStatement(DELETE_NEWS);
            stmt.setLong(1, id);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public News select(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        News news = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT_NEWS);
            stmt.setLong(1, id);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    news = new News(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getTimestamp(5), resultSet.getDate(6));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return news;
    }

    
    public ArrayList<News> selectAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<News> arrayList = new ArrayList<News>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL_NEWS)) != null) {
                while (resultSet.next()) {
                    arrayList.add(new News(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getTimestamp(5), resultSet.getDate(6)));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return arrayList;
    }

    
    public void update(News obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(UPDATE_NEWS);
            stmt.setString(1, obj.getTitle());
            stmt.setString(2, obj.getShortText());
            stmt.setString(3, obj.getFullText());
            stmt.setTimestamp(4, obj.getCreationDate());
            stmt.setDate(5, obj.getModificationDate());
            stmt.setLong(6, obj.getId());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public Long countNews() {
        Connection conn = null;
        PreparedStatement stmt = null;
        Long countNews = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(NEWS_COUNT);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    countNews = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return countNews;
    }

    
    public void addTagLink(Long newsId, Long tagId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(ADD_TAG_TO_NEWS);
            stmt.setLong(1, newsId);
            stmt.setLong(2, tagId);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public void deleteTagLink(Long newsId, Long tagId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_TAG_FROM_NEWS);
            stmt.setLong(1, newsId);
            stmt.setLong(2, tagId);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public void addAuthorLink(Long newsId, Long authorId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(ADD_AUTHOR_TO_NEWS);
            stmt.setLong(1, newsId);
            stmt.setLong(2, authorId);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public void deleteAuthorLink(Long newsId, Long authorId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_AUTHOR_FROM_NEWS);
            stmt.setLong(1, newsId);
            stmt.setLong(2, authorId);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    public void addTagLinks(Long newsId, List<Long> tags) throws DAOException {
        Iterator<Long> iterator = tags.iterator();
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(ADD_TAG_TO_NEWS);
            while (iterator.hasNext()) {
                stmt.setLong(1, newsId);
                stmt.setLong(2, iterator.next());
                stmt.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public List<News> search(SearchCriteria searchCriteria) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news");
        ArrayList<News> arrayList = new ArrayList<News>();
        if (searchCriteria.getAuthor() != null) {
            stringBuilder.append(" WHERE news_id IN (SELECT news_id FROM news_author WHERE author_id=?) ");
        }
        if (searchCriteria.getTags() != null && searchCriteria.getTags().size() != 0) {
            if (searchCriteria.getAuthor() != null) {
                stringBuilder.append(" AND ");
            } else {
                stringBuilder.append(" WHERE ");
            }
            stringBuilder.append("news_id IN (SELECT news_id FROM news_tag WHERE tag_id IN (");
            Iterator<Tag> iterator = searchCriteria.getTags().iterator();
            stringBuilder.append(iterator.next().getId());
            while (iterator.hasNext()) {
                stringBuilder.append(", " + iterator.next().getId());
            }
            stringBuilder.append("))");
        }
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(stringBuilder.toString());
            if (searchCriteria.getAuthor() != null) {
                stmt.setLong(1, searchCriteria.getAuthor().getId());
            }
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                arrayList.add(new News(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getTimestamp(5), resultSet.getDate(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return arrayList;
    }

    
    public List<News> getTopNews(List<Long> newsId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        ArrayList<News> arrayList = new ArrayList<News>();
        Iterator<Long> iterator = newsId.iterator();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT_NEWS);
            while (iterator.hasNext()) {
                stmt.setLong(1, iterator.next());
                resultSet = stmt.executeQuery();
                if (resultSet.next())
                    arrayList.add(new News(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getTimestamp(5), resultSet.getDate(6)));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return arrayList;
    }
}
