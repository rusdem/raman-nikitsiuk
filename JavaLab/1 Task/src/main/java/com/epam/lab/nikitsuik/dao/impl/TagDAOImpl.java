package com.epam.lab.nikitsuik.dao.impl;

import com.epam.lab.nikitsuik.dao.TagDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TagDAOImpl implements TagDAO {
    @Autowired
    private DriverManagerDataSource dataSource = null;

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final String ADD = "INSERT INTO tag (tag_id, tag_name) VALUES (TAG_AUTOINCREMENT.nextval,?)";
    private static final String DELETE = "DELETE FROM tag WHERE tag_id=?";
    private static final String SELECT = "SELECT tag_id, tag_name FROM tag WHERE tag_id=?";
    private static final String SELECT_ALL = "SELECT tag_id, tag_name FROM tag ORDER BY tag_id";
    private static final String UPDATE = "UPDATE tag SET tag_name=? WHERE tag_id=?";
    private static final String DELETE_TAG_LINK = "DELETE FROM news_tag WHERE tag_id=?";


    
    public Long add(Tag obj) {
        Long resultId = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            int generatedColomns[] = {1};
            stmt = conn.prepareStatement(ADD, generatedColomns);
            stmt.setString(1, obj.getTagName());
            stmt.executeUpdate();
            ResultSet resultSet = stmt.getGeneratedKeys();
            resultSet.next();
            resultId = resultSet.getLong(1);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return resultId;
    }

    
    public void delete(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_TAG_LINK);
            stmt.setLong(1, id);
            stmt.execute();
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public Tag select(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Tag tag = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT);
            stmt.setLong(1, id);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    tag = new Tag(resultSet.getLong(1), resultSet.getString(2));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return tag;
    }

    
    public ArrayList<Tag> selectAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<Tag> arrayList = new ArrayList<Tag>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL)) != null) {
                while (resultSet.next()) {
                    arrayList.add(new Tag(resultSet.getLong(1), resultSet.getString(2)));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return arrayList;
    }

    
    public void update(Tag obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(UPDATE);
            stmt.setString(1, obj.getTagName());
            stmt.setLong(2, obj.getId());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public List<Long> addTags(List<Tag> tags) {
        List<Long> resultsId = new ArrayList<Long>(tags.size());
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            int generatedColomns[] = {1};
            stmt = conn.prepareStatement(ADD, generatedColomns);
            Iterator<Tag> iterator = tags.iterator();
            ResultSet resultSet = null;
            while (iterator.hasNext()) {
                stmt.setString(1, iterator.next().getTagName());
                stmt.executeUpdate();
                resultSet = stmt.getGeneratedKeys();
                resultSet.next();
                resultsId.add(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn,dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return resultsId;
    }
}
