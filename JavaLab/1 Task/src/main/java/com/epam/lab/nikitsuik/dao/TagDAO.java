package com.epam.lab.nikitsuik.dao;

import com.epam.lab.nikitsuik.entity.Tag;
import com.epam.lab.nikitsuik.dao.exception.*;

import java.util.List;

public interface TagDAO extends TemplateDAO<Tag> {
    /**
     * Add group of tags
     * @param tags
     * @return list of generated id in database to added tags
     * @throws DAOException
     */
    public List<Long> addTags(List<Tag> tags) throws DAOException;
}
