package com.epam.lab.nikitsuik.service;

import com.epam.lab.nikitsuik.dao.UserTableDAO;
import com.epam.lab.nikitsuik.service.exception.ServiceException;
import com.epam.lab.nikitsuik.entity.UserTable;

import java.util.ArrayList;

public interface UserTableService {
    public void setUserTableDAO(UserTableDAO userTableDAO);

    public Long add(UserTable obj) throws ServiceException;

    public void delete(Long id) throws ServiceException;

    public UserTable select(Long id) throws ServiceException;

    public ArrayList<UserTable> selectAll() throws ServiceException;

    public void update(UserTable obj) throws ServiceException;
}
