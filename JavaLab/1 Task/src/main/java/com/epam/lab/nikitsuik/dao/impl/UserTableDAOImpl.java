package com.epam.lab.nikitsuik.dao.impl;

import com.epam.lab.nikitsuik.dao.UserTableDAO;
import com.epam.lab.nikitsuik.dao.exception.DAOException;
import com.epam.lab.nikitsuik.entity.UserTable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;

public class UserTableDAOImpl implements UserTableDAO {
    @Autowired
    private DriverManagerDataSource dataSource = null;

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final String ADD = "INSERT INTO user_table (user_id, login, password, user_name) VALUES (USER_AUTOINCREMENT.nextval,?,?,?)";
    private static final String DELETE = "DELETE FROM user_table WHERE user_id=?";
    private static final String SELECT = "SELECT user_id, login, password, user_name FROM user_table WHERE user_id=?";
    private static final String SELECT_ALL = "SELECT user_id, login, password, user_name FROM user_table ORDER BY user_id";
    private static final String UPDATE = "UPDATE user_table SET login=?, password=?, user_name=? WHERE user_id=?";
    private static final String DELETE_ROLES = "DELETE FROM roles WHERE user_id=?";
    
    public Long add(UserTable obj) {
        Long resultId = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            int generatedColomns[] = {1};
            stmt = conn.prepareStatement(ADD, generatedColomns);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getPassword());
            stmt.setString(3, obj.getUserName());
            stmt.executeUpdate();
            ResultSet resultSet = stmt.getGeneratedKeys();
            resultSet.next();
            resultId = resultSet.getLong(1);
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return resultId;
    }

    
    public void delete(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(DELETE_ROLES);
            stmt.setLong(1, id);
            stmt.execute();
            stmt = conn.prepareStatement(DELETE);
            stmt.setLong(1, id);
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }

    
    public UserTable select(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        UserTable userTable = null;
        ResultSet resultSet = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(SELECT);
            stmt.setLong(1, id);
            if ((resultSet = stmt.executeQuery()) != null) {
                if (resultSet.next()) {
                    userTable = new UserTable(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return userTable;
    }

    
    public ArrayList<UserTable> selectAll() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        ArrayList<UserTable> arrayList = new ArrayList<UserTable>();
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.createStatement();
            if ((resultSet = stmt.executeQuery(SELECT_ALL)) != null) {
                while (resultSet.next()) {
                    arrayList.add(new UserTable(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
                }
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return arrayList;
    }

    
    public void update(UserTable obj) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DataSourceUtils.getConnection(dataSource);
            stmt = conn.prepareStatement(UPDATE);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getPassword());
            stmt.setString(3, obj.getUserName());
            stmt.setLong(4, obj.getId());
            stmt.execute();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    DataSourceUtils.releaseConnection(conn, dataSource);
                }
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
    }
}
